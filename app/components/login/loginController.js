(function(){
    var app = angular.module('comicStore');

    app.controller("loginController", function($location, $cookieStore, $http, $rootScope, UserService) {
    	this.user = {};
        this.error = false;
        this.message = "";

    	this.doLogin = function() {

            this.error = false;
            this.message = "";
    		var userfromdb = UserService.getByUsername(this.user.formUser);

    		if(userfromdb !== null && userfromdb.password === this.user.formPassword) {

    			var authdata = this.user.formUser + ':' + userfromdb.password;
                var authenticatedUser = {
                    username: this.user.formUser,
                    authdata: authdata,
                    role: userfromdb.role
                };

                localStorage.authenticatedUser = JSON.stringify(authenticatedUser);

                $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
            	$location.path('/');
    		} else {
                this.error = true;
                this.message = "User or password incorrect";
    		}
    	};
    });

})();
