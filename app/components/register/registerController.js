(function(){
    var app = angular.module('comicStore');

    app.controller("registerController", ['UserService', "$location", function(UserService, $location) {

    	this.user = {role: "user"};
    	this.success = false;
        this.error = false;
    	this.message = "";

    	this.doRegister = function() {
            this.success = false;
            this.error = false;
            this.message = "";
    		var response = UserService.create(this.user);
    		if(response.success) {
    			this.success = true;
                this.user = {role: "user"};
    		} else {
    			this.error = true;
    			this.message = response.message;
    		}
    	};
    }]);

})();