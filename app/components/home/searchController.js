(function(){
    var app = angular.module('comicStore');


    app.controller('searchController', function($filter){

        this.criteria = {
                name: [],
                genres: [],
                editions: [],
                characters: [],
                filter: false
            };

        this.showSearchCriteria = false;

        this.genres = JSON.parse(localStorage.genres);
        this.editions = JSON.parse(localStorage.editions);
        this.characters = JSON.parse(localStorage.characters);

        this.comics = JSON.parse(localStorage.comics);

        var comicByRating = $filter('orderBy')(this.comics, 'rating', true);

        this.topRatings = $filter('limitTo')(comicByRating, 3);

        this.isCriteriaEmpty = function() {
            return this.criteria.name.length === 0 && 
            this.criteria.genres.length === 0 && 
            this.criteria.editions.length === 0 && 
            this.criteria.characters.length === 0;
        };


        this.addCriteria = function(key, value) {
            
            if(!angular.isUndefined(this.criteria[key])) {
                var duplicate = $filter('filter')(this.criteria[key], value);

                if(duplicate.length === 0) {
                    this.criteria[key].push(value);
                    this.showSearchCriteria = true;
                    this.criteria.filter = true;    
                }
            }
        };

        this.resetCriteria = function() {
            this.criteria = {
                name: [],
                genres: [],
                editions: [],
                characters: [],
                filter: false
            };
            this.showSearchCriteria = false;
        };

        this.removeCriteria = function(key, value) {
            if(!angular.isUndefined(this.criteria[key])) {
                var index = this.criteria[key].indexOf(value);
                this.criteria[key].splice(index, 1);
            }
            if(this.isCriteriaEmpty()) {
                this.resetCriteria();
            }
        };

        this.getCriteriaName = function(key, value) {

            var val = "";
            
            if(key === 'genres') {
                var genre = $filter('filter')(this.genres, {id: value})
                
                val = genre[0].name;
            } else if(key === 'editions') {
                var edition = $filter('filter')(this.editions, {id: value})
                
                val = edition[0].name;
            } else if (key === 'characters') {
                var ch = $filter('filter')(this.characters, {id: value})
                
                val = ch[0].name;
            }
            
            return val;
        }
    });

})();
