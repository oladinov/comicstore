(function(){
    var app = angular.module('comicStore');


    app.controller('userPageController', function(UserService, $location) {
    	var authenticatedUser = JSON.parse(localStorage.authenticatedUser);

    	this.user = UserService.getByUsername(authenticatedUser.username);

    	this.users = UserService.getAll();

    	this.admin = this.user.role === 'admin';

    	this.noOfUsers = this.users.length;

        this.newPassword = {
            oldPassword: "",
            newPassword: ""
        };

        this.incorrectPassword = false;

    	this.isAdmin = function(user) {
    		return user.role === "admin";
    	};

    	this.update = function(user, newRole) {
    		user.role = newRole
    		UserService.update(user);
    		this.users = UserService.getAll();
    	};

        this.updateName = function() {
            UserService.update(this.user);
            this.users = UserService.getAll();
            alert("Your user has been updated. You will now be redirected to the home page.");
            $location.path('/');
        };

        this.updatePassword = function() {
            if(this.newPassword.oldPassword === this.user.password) {
                this.user.password = this.newPassword.newPassword;
                UserService.update(this.user);
                alert("Your user has been updated. You will now be redirected to the home page.");
                $location.path('/');
            } else {
                alert('The password is incorrect.');
                this.incorrectPassword = true;
            }
        };

    	this.delete = function(username) {
    		if(confirm("Are you sure you want to delete the user " + username + "?")) {
				UserService.delete(username);
    			this.users = UserService.getAll();
    		}
    		
    	};

    	this.doLogOut = function() {
    		localStorage.authenticatedUser = null;
    		$location.path('/login');
    	};
    });
})();
