(function(){
    var app = angular.module('comicStore');

    app.controller('homeUserController', function(UserService, $location) {

    	this.username =  JSON.parse(localStorage.authenticatedUser).username;

    	this.user = UserService.getByUsername(this.username);

        this.admin = this.user.role === 'admin';

    	this.users = UserService.getAll();

    	this.doLogOut = function() {
    		localStorage.authenticatedUser = null;
    		$location.path('/login');
    	};
    });

})();
