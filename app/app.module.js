(function() {
  var app = angular.module('comicStore', ['ngRoute', 'ngCookies']);

  app.config(function($routeProvider, $locationProvider) {
  	$routeProvider.
  		when('/', {
  			templateUrl: './home/home.html',
        controller: 'searchController',
        controllerAs: "searchCtrl"
  		})
  		.when('/login', {
  			templateUrl: './inicio.html'
  		})
  		.when('/register', {
  			templateUrl: './register.html'
  		})
      .when('/user', {
        templateUrl: './home/user.html'
      })
      .when('/disclaimer', {
        templateUrl: './home/disclaimer.html'
      })
      .when('/sitemap', {
        templateUrl: './home/sitemap.html'
      })
      .when('/admin', {
        templateUrl: './home/manageUsers.html'
      })
      .when('/404', {
        templateUrl: './404.html'
      })
  		.otherwise({redirecTo: '/404'});

    //$locationProvider.html5Mode(true)
  });

  app.run(function($rootScope, $location, $http) {

        if(!localStorage.users) {
          var fileLocation1= window.location.pathname + 'app/json/users.json';

          $http.get(fileLocation1).then(function(response) {
            localStorage.users = JSON.stringify(response.data);
          });
        }

        if(!localStorage.genres) {
          var fileLocation2= window.location.pathname + 'app/json/genres.json';
          $http.get(fileLocation2).then(function(response) {
            localStorage.genres = JSON.stringify(response.data);
          });
        }

        if(!localStorage.editions) {
          var fileLocation3= window.location.pathname + 'app/json/edition.json';
          $http.get(fileLocation3).then(function(response) {
            localStorage.editions = JSON.stringify(response.data);
          });
        }
        
        if(!localStorage.characters) {
          var fileLocation4= window.location.pathname + 'app/json/characters.json';
          $http.get(fileLocation4).then(function(response) {
            localStorage.characters = JSON.stringify(response.data);
          });
        }

        if(!localStorage.comics) {
          var fileLocation5= window.location.pathname + 'app/json/comics.json';
          $http.get(fileLocation5).then(function(response) {
            localStorage.comics = JSON.stringify(response.data);
          });
        }

        $rootScope.$on('$routeChangeStart', function (ev, to, toParams, from, fromParams) {
            var authenticatedUser = JSON.parse(localStorage.authenticatedUser ? localStorage.authenticatedUser : null);

            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = ['/login', '/register'].indexOf($location.path()) === -1;
            var loggedIn = authenticatedUser !== null;
            if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }
        });
    });


})();
