(function(){
    var app = angular.module('comicStore');

    app.filter('filterComics',['$filter',function ($filter) {
	    return function (items, keyObj) {

            if(!keyObj.filter) {
                return items;
            }
            
    	    var filterObj = {
                            data:items,
                            filteredData:[],
                            filtered: false,
                            applyFilter : function(obj,key){
                                var fData = [];
                                if(this.filteredData.length == 0 && !this.filtered) this.filteredData = this.data;
                                if(obj){
                                    var fObj = {};
                                    if(angular.isString(obj)){
                                        fObj[key] = obj;
                                        fData = fData.concat($filter('filter')(this.filteredData,fObj));
                                    } else if(angular.isArray(obj)){
                                        if(obj.length > 0){ 
                                            for(var i=0;i<obj.length;i++){
                                                if(angular.isString(obj[i])){
                                                    fObj[key] = obj[i];
                                                    fData = fData.concat($filter('filter')(this.filteredData,fObj));    
                                                }
                                            }
                                            
                                        } else {
                                            //No filtering where performed.
                                            return;
                                        }
                                    } else {
                                        //No filtering where performed.
                                        return;
                                    }
                                    this.filteredData = fData;
                                    this.filtered = true;
                                } //The object to search is not defined or null
                            }
                };

        	if(keyObj){
            	angular.forEach(keyObj,function(obj,key){
                	filterObj.applyFilter(obj,key);
            	});         
        	}
       
        	return filterObj.filteredData;
    	}
	}]);

})();
