(function() {
  var app = angular.module('comicStore');

  app.factory('UserService', ['$filter', '$q', function($filter, $q) {
  	return  {
  		getAll: function() {
  			return this.getUsers();
  		},
  		getByUsername: function(username) {
			var filtered = $filter('filter')(this.getUsers(), {username: username});
  			var user = filtered.length ? filtered[0] : null;
  			return user;
  		},
  		create: function(user) {
  			var duplicated = this.getByUsername(user.username);
  			if(duplicated !== null) {
  				return {success: false, message: 'Username "' + user.username + '" is already taken'};
  			} else {
  				var users = this.getUsers();
  				users.push(user);
  				this.setUsers(users);
				return { success: true };
  			}
  		},
  		update: function(user) {
  			var users = this.getUsers();
            for (var i = 0; i < users.length; i++) {
                if (users[i].username === user.username) {
                    users[i] = user;
                    break;
                }
            }
            this.setUsers(users);
  		},
  		delete: function(username) {
			var users = this.getUsers();
            for (var i = 0; i < users.length; i++) {
                var user = users[i];
                if (user.username === username) {
                    users.splice(i, 1);
                    break;
                }
            }
            this.setUsers(users);
  		},
  		getUsers: function() {
  			if(!localStorage.users) {
  				localStorage.users = JSON.stringify([]);
  			}

  			return JSON.parse(localStorage.users);
  		},
  		setUsers: function(users) {
  			localStorage.users = JSON.stringify(users);
  		}
  	};
  }]);
})();