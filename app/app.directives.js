(function(){
    var app = angular.module('comicStore');    

    app.directive('userregister', function() {
    	return {
    		require: 'ngModel',
    		link: function(scope, elm, attrs, ctrl) {
    			ctrl.$validators.username = function(modelValue, viewValue) {
    				if(/[^a-zA-Z0-9]/.test(modelValue)) {
    					return false;
    				}
    				return true;
    			}
    		}
    	};
    });

    app.directive('passregister', function() {
    	return {
    		require: 'ngModel',
    		link: function(scope, elm, attrs, ctrl) {
    			ctrl.$validators.password = function(modelValue, viewValue) {

    				if(modelValue && modelValue.length < 7) {
    					return false;
    				}

    				return true;
    			}
    		}
    	};
    });

    app.directive('password', function() {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$validators.password = function(modelValue, viewValue) {

                    if(modelValue && modelValue.length < 7) {
                        return false;
                    }

                    return true;
                }
            }
        };
    });

    app.directive('username', function() {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$validators.username = function(modelValue, viewValue) {
                    if(/[^a-zA-Z0-9]/.test(modelValue)) {
                        return false;
                    }

                    return true;
                }
            }
        };
    });

    app.directive('starRating', function () {
        return {
            restrict: 'A',
            template: '<ul class="rating">' +
                '<li ng-repeat="star in stars" ng-class="star">' +
                '\u2605' +
                '</li>' +
                '</ul>',
            scope: {
                ratingValue: '=',
                max: '='
            },
            link: function (scope, elem, attrs) {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            }
        }
    });


    app.directive('product', function(){
        return {
            restrict: "E",
            templateUrl: './home/comicbook.html',
            controller: function() {
                
            },
            controllerAs: "productCtrl"
        };
    });



    app.directive('comicheader', function() {
        return {
            restrict: "E",
            scope: {
                controller: '='
            },
            templateUrl: './home/header.html',
            controller: function($scope) {
                this.genres = JSON.parse(localStorage.genres);
                this.editions = JSON.parse(localStorage.editions);
                this.characters = JSON.parse(localStorage.characters);

                this.searchName = "";
            },
            controllerAs: "headCtrl"
        }; 
    });

    app.directive('footerComics', function() {
        return {
            restrict: "E",
            templateUrl: './home/footer.html'
        };
    });

})();
